export const state = () => ({
  productsInCart: [],
  productsIdInCart: [],
  productsTotalPrice: 0,
})

export const actions = {
  cart({ commit }, data) {
    commit('setProductToCart', data)
  },
  addToCart({ commit }, card) {
    // const cart = JSON.parse(sessionStorage.getItem('cart'))
    // if (cart) {
    //   console.log(15)
    commit('ADD_TO_CART', card)
    //   sessionStorage.setItem('cart', JSON.stringify(state.productsInCart))
    // } else if (!state.productsInCart && !cart) {
    //   console.log(19)
    //   commit('ADD_TO_CART', card)
    //   sessionStorage.setItem('cart', JSON.stringify())
    // } else {
    //   console.log(24)
    //   commit('ASSIGN_CART', card)
    // }
  },
  assignCart({ commit }, card) {
    commit('ASSIGN_CART', card)
  },
  assignCartIds({ commit }, card) {
    commit('ASSIGN_CART_IDS', card)
  },
  deleteFromCart({ commit }, card) {
    commit('DELETE_FROM_CART', card)
  },
  productCountUp({ commit }, card) {
    commit('PRODUCT_COUNT_UP', card)
  },
  productCountDown({ commit }, card) {
    commit('PRODUCT_COUNT_DOWN', card)
  },
  clearCart({ commit }) {
    commit('CLEAR_CART')
  },
}

export const mutations = {
  ADD_TO_CART(state, card) {
    state.productsInCart.push(card)
    state.productsIdInCart.push(card.id)
    // const cartData = JSON.parse(sessionStorage.getItem('cart')) ? JSON.parse(sessionStorage.getItem('cart')) : []
    // cartData.push(card)
    // sessionStorage.setItem('cart', JSON.stringify(cartData))
  },
  ASSIGN_CART(state, cart) {
    state.productsInCart = cart
  },
  ASSIGN_CART_IDS(state, cart) {
    state.productsIdInCart = cart
  },
  DELETE_FROM_CART(state, card) {
    const productsInCart = state.productsInCart.filter((f) => f.id !== card.id)
    const productsIdInCart = state.productsIdInCart.filter((f) => f !== card.id)
    state.productsInCart = productsInCart
    state.productsIdInCart = productsIdInCart
    // const productsInCart = state.productsInCart.filter((f) => f.id !== card.id)
    // sessionStorage.setItem('cart', JSON.stringify(productsInCart))
  },
  PRODUCT_COUNT_UP(state, card) {
    state.productsInCart.forEach((element) => {
      if (element.id === card) {
        element.product_count++
      }
    })
  },
  PRODUCT_COUNT_DOWN(state, card) {
    state.productsInCart.forEach((element) => {
      if (element.id === card) {
        if (element.product_count > 1) {
          element.product_count--
        }
      }
    })
  },
  CLEAR_CART(state) {
    state.productsInCart = []
    state.productsIdInCart = []
  },
}

export const getters = {
  productsInCart: (state) => state.productsInCart,
  productsIdInCart: (state) => state.productsIdInCart,
  productTotalPrice: (state) => {
    let totalPrice = 0
    state.productsInCart.forEach((el) => {
      totalPrice += el.totalPrice
    })
    return totalPrice
  },
}
