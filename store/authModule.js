export const state = () => ({
  token: null,
  list: {},
})

export const getters = {
  hasToken: (state) => !!state.token,
  userData: (state) => state.list,
}

export const actions = {
  login({ commit }, data) {
    commit('setData', data)
  },
}

export const mutations = {
  setToken(state, data) {
    state.token = data
  },
  setData(state, data) {
    if (process.browser) {
      sessionStorage.setItem('token', data.key)
      sessionStorage.setItem('userData', JSON.stringify(data))
      state.list =
        process.browser && sessionStorage.getItem('userData') !== null
          ? JSON.parse(sessionStorage.getItem('userData'))
          : data
      state.token = sessionStorage.getItem('token')
    }
  },
}
