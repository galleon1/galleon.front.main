module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: ['@nuxtjs', 'plugin:prettier/recommended', 'plugin:nuxt/recommended', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'no-undef': 0,
    'no-irregular-whitespace': 0,
    'prettier/prettier': ['error', { endOfLine: 'auto' }],
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/attribute-hyphenation': 'off',
    'object-shorthand': 'off',
  },
}
  