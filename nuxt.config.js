import { baseURL, mapAPI } from './baseURL'
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'galleon',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { src: `https://maps.googleapis.com/maps/api/js?key=${mapAPI}&libraries=places&callback=initMap` },
      {
        src: 'https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/styles/main.scss'],

  styleResources: {
    scss: [
      '~/assets/styles/mixins.scss',
      '~assets/styles/vars.scss',
      '~/assets/styles/general.scss',
      '~/assets/styles/form.scss',
    ],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/Vuelidate' },
    { src: '~/plugins/vue-mask.js' },
    { src: '~/plugins/functions.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxtjs/vuetify', '@nuxtjs/device'],

  modules: ['@nuxtjs/eslint-module', '@nuxtjs/axios', '@nuxtjs/style-resources'],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: baseURL,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
