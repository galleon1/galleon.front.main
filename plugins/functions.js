const globalFunctions = {
  name: 'globalFunctions',
  getStatusString(item) {
    switch (item) {
      case 1:
        return 'Новый'
      case 2:
        return 'В обработке'
      case 3:
        return 'Доставлен'
      case 4:
        return 'Возврат'
    }
  },
  getTotalPrice(array) {
    let totalPrice = 0
    array.forEach((element) => {
      totalPrice += element.product.price * element.count
    })
    return totalPrice
  },
  getPaymentString(item) {
    switch (item) {
      case 1:
        return 'Оплата наличными'
      case 2:
        return 'Картой Visa/MasterCard'
      case 3:
        return 'Оплата в системе Kaspi.kz'
    }
  },
  getDeliveryString(item) {
    switch (item) {
      case 1:
        return 'Самовывоз'
      case 2:
        return 'Доставка'
    }
  },
  // getHistoryDateFormat(item) {
  //   IPOdate.setTime(Date.parse('Aug 9, 1995'))
  //   console.log(Date.parse(item))
  // },
}

export default ({ app }, inject) => {
  inject('globalFunctions', globalFunctions)
}
